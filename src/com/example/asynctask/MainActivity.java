package com.example.asynctask;

import java.util.concurrent.ExecutionException;

import org.json.JSONArray;

import com.example.asynctask.task.Helper;

import android.os.Bundle;
import android.app.Activity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {
	
	private TextView lblResult;
	private EditText txtTexto;
	private Button btn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lblResult = (TextView)findViewById(R.id.lblResult);
		txtTexto = (EditText)findViewById(R.id.txtTexto);
		btn = (Button)findViewById(R.id.btn);
		
		btn.setOnClickListener( new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					
					String resultado = new Helper().execute("http://observatorio.e-frovel.com/apis/getMarcadores").get();
					Log.i("JSON OBTENIDO", resultado);
					
					
				} catch (InterruptedException e) {

				} catch (ExecutionException e) {

				}
				
			}
		});
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
