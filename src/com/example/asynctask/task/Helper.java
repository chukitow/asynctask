package com.example.asynctask.task;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import android.os.AsyncTask;
import android.util.Log;

public class Helper extends AsyncTask<String, String, String> {

	@Override
	protected String doInBackground(String... params) {
		
		String json = null;
		
		
		try {
			HttpClient cleinte = new DefaultHttpClient();
			HttpGet GET = new HttpGet(params[0]);
			HttpResponse respuesta = cleinte.execute(GET);
			HttpEntity ent = respuesta.getEntity();
			json = EntityUtils.toString(ent);
		} catch (ClientProtocolException e) {
			Log.i("exepcion lanzada", "buh");
			e.printStackTrace();
		} catch (IOException e) {

		}
		
		return json;
		
	}

}
